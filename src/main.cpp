#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <ctime>
#include "globals.h"
#include "randomc.h"
#include "chromosome.h"
#include "population.h"
#include "problem.h"

using namespace std;

int main(int argc, char* argv[]){
	if(argc != 2){
		string command = argv[0];
		size_t slashPosition = command.find_last_of('/');
		if (slashPosition != string::npos) {
			command = command.substr(slashPosition + 1);
		}
		cout << "Usage: " << command << " RANDOM_SEED" << endl;
		return 0;
	}

	SEED = atoi(argv[1]);
	//problemNo = atoi(argv[2]);	//ZDT1: 1, ZDT2: 2, ZDT3: 3, ZDT4: 4, ZDT5: 5
	//int popsize = atoi(argv[3]);
	//double eps = atof(argv[4]);
	//int gens = atoi(argv[5]);

	int popsize = 0;
	double eps = 0.0;
	int gens = 0;
	int lev = 0;
	cout << "Problem: " << endl;
	cin >> problemNo;
	cout << "Population size: "<< endl;
	cin >> popsize;
	cout << "Desired Epsilon: " << endl;
	cin >> eps;
	cout << "Generations: " << endl;
	cin >> gens;
	cout << "Levels: " << endl;
	cin >> lev;

	rg.RandomInit(SEED);
	setProblem(problemNo);
	setCoef();
	setParams(lev);

	clock_t begin = clock();
	Population poplist(popsize, eps);

//	double GENMULTIPLIER = 5;
	
	int curLevel = 1;
	int updatePoint = gens / 3;
	int rest = gens / 6;

	for(int iter = 0; iter != gens; ++iter){
		vector<Chromosome> offspring = poplist.chooseArc().mate(poplist.choosePop());
		for(std::vector<Chromosome>::iterator i = offspring.begin(); i != offspring.end(); ++i){
			bool inserted = poplist.evaluatePop(*i);
			if(inserted == true){
				poplist.evaluateArc(*i);
			}
		}
		if(iter == updatePoint){
			cout << "Current Generation: " << iter << endl;
			poplist.update(curLevel);
			++curLevel;
			if(curLevel < LEVELS){
//				updatePoint += static_cast<int>((gens - updatePoint) / GENMULTIPLIER);		//schedule a new update point
				updatePoint += static_cast<int>((gens - updatePoint - rest) / (LEVELS - curLevel + 1));		//schedule a new update point
			}
			else{
				updatePoint = 2 * gens;		//Never update again
			}
		}
	}
	clock_t end = clock();
	ofstream tim("TIM");
	tim << end - begin;
	ofstream fun("FUN");
	poplist.printArc(fun);
	ofstream var("VAR");
	poplist.printArcVar(var);

	tim.close();
	fun.close();
	var.close();


	cout << endl;
	Chromosome best = poplist.getBest();
	for(int i = 0; i != NOBJ; ++i){
		cout << "Obj " << i << ": " << best.getObj(i) << "\tWeight " << i << ": " << best.getWeight(i) << endl;
	}

	ofstream bestf("BFUN");
	for(int i = 0; i != NOBJ; ++i){
		bestf << best.getObj(i) << " ";
	}

	ofstream bestw("BWEIGHT");
	for(int i = 0; i != NOBJ; ++i){
		bestw << best.getWeight(i) << " ";
	}

	bestf.close();
	bestw.close();

	cout << endl << "Filtered Case:" << endl;
	best = poplist.getFilteredBest();
	for(int i = 0; i != NOBJ; ++i){
		cout << "Obj " << i << ": " << best.getObj(i) << "\tWeight " << i << ": " << best.getWeight(i) << endl;
	}

	bestf.open("BFUNF");
	for(int i = 0; i != NOBJ; ++i){
		bestf << best.getObj(i) << " ";
	}

	bestw.open("BWEIGHTF");
	for(int i = 0; i != NOBJ; ++i){
		bestw << best.getWeight(i) << " ";
	}

	bestf.close();
	bestw.close();

	cout << endl << end - begin << endl;

	return 0;
}
