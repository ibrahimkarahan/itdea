#include "population.h"

double EPSMULTIPLIER = 1;
double DEFAULT = 0.1;

Population::Population(int popSize, double eps){
	EPSMULTIPLIER = log(DEFAULT / eps) / (LEVELS - 1);
	this->epsilons.push_back(std::exp(EPSMULTIPLIER * (LEVELS - 1)) * eps);

//	EPSMULTIPLIER = (DEFAULT - eps) / static_cast<double>(LEVELS - 1);
//	this->epsilons.push_back(EPSMULTIPLIER * (LEVELS - 1) + eps);
	
	this->intervals.push_back(std::vector<couple>());
	for(int i = 0; i != NOBJ; ++i){
		couple temp(0.0, 1.0);
		this->intervals.at(0).push_back(temp);
	}

	for(int i = 0; i != popSize; ++i){
		Chromosome input;
		this->pop.push_back(input);
	}

	std::vector<bool> dominated((this->pop).size(), 0);
	int sol1 = 0;
	for(std::list<Chromosome>::iterator i = (this->pop).begin(); i != ((this->pop).end()); ++i){
		if(!dominated.at(sol1)){
			std::list<Chromosome>::iterator beginIter = i;
			++beginIter;
			int sol2 = sol1 + 1;
			for(std::list<Chromosome>::iterator j = beginIter; j != (this->pop).end(); ++j){
				if(!dominated.at(sol2)){
					int whichOne = i->checkDom(*j);
					if(whichOne == 1){
						dominated.at(sol2) = 1;
					}
					else if(whichOne == 2){
						dominated.at(sol1) = 1;
					}
				}
				++sol2;
			}
		}
		++sol1;
	}
	int current = 0;
	for(std::list<Chromosome>::iterator i = (this->pop).begin(); i != (this->pop).end(); ++i){
		if(!dominated.at(current)){
			this->archive.push_back(*i);
		}
		++current;
	}
}

const Chromosome& Population::choosePop() const{
	int random1 = static_cast<int>((this->pop).size() * rg.Random());
	int random2 = static_cast<int>((this->pop).size() * rg.Random());

	std::list<Chromosome>::const_iterator first = (this->pop).begin();
	for(int i = 0; i != random1; ++i){
		++first;
	}
	std::list<Chromosome>::const_iterator second = (this->pop).begin();
	for(int i = 0; i != random2; ++i){
		++second;
	}

	int dominating = first->checkDom(*second);
	if(dominating == 1){
		return *first;
	}
	else if(dominating == 2){
		return *second;
	}
	else {
		if(rg.Random() < 0.5){
			return *first;
		}
		else {
			return *second;
		}
	}
}

const Chromosome& Population::chooseArc() const{
	int random = static_cast<int>((this->archive).size() * rg.Random());
	std::list<Chromosome>::const_iterator iter = (this->archive).begin();
	for(int i = 0; i != random; ++i){
		++iter;
	}
	return *iter;
}

bool Population::evaluatePop(const Chromosome& input){
	std::vector<int> domIndex;
	int current = 0;
	for(std::list<Chromosome>::iterator i = (this->pop).begin(); i != (this->pop).end(); ++i){
		int j = input.checkDom(*i);
		if(j == 0){
			if(input == *i){
				return 0;
			}
		}
		if(j == 1){
			domIndex.push_back(current);
		}
		if(j == 2){
			return 0;
		}
		++current;
	}
	if(!(this->pop).empty()){
		int random = static_cast<int>(domIndex.size() * rg.Random());
		std::list<Chromosome>::iterator iter = (this->pop).begin();
		for(int i = 0; i != random; ++i){
			++iter;
		}
		(this->pop).erase(iter);
	}
	(this->pop).push_back(input);
	return 1;
}

void Population::evaluateArc(const Chromosome& input){
	std::vector<int> eraseList((this->archive).size(), 0);
	int current = 0;
	bool noInsert = false;
	for(std::list<Chromosome>::iterator i = (this->archive).begin(); i != (this->archive).end(); ++i){
		if(input == *i){
			return;
		}
		int domCheck = input.checkDom(*i);
		if(domCheck == 1){
			eraseList.at(current) = 1;
		}
		else if(domCheck == 2){
			return;
		}
		++current;
	}
	std::list<Chromosome>::iterator iter = (this->archive).begin();
	for(std::vector<int>::iterator i = eraseList.begin(); i != eraseList.end(); ++i){
		if(*i == 1){
			iter = (this->archive).erase(iter);
		}
		else {
			++iter;
		}
	}

	if(!(this->archive.empty())){
		double diff = 100000;
		std::list<Chromosome>::iterator elem = this->archive.begin();
		for(std::list<Chromosome>::iterator i = this->archive.begin(); i != this->archive.end(); ++i){
			double temp = 0;
			for(int j = 0; j != NOBJ; ++j){
				temp += fabs(i->getSObj(j) - input.getSObj(j));
			}
			if(temp < diff){
				elem = i;
				diff = temp;
			}
		}
		double maxObjVal = 0;
		for(int i = 0; i != NOBJ; ++i){
			double tempObjVal = fabs(elem->getSObj(i) - input.getSObj(i));
			if(tempObjVal > maxObjVal){
				maxObjVal = tempObjVal;
			}
		}

		double curEps = this->epsilons.at(0);
		int curInt = 0;
		for(std::vector<std::vector<couple> >::iterator i = this->intervals.begin(); i != this->intervals.end(); ++i){
			bool flag = true;
			for(int j = 0; j != NOBJ; ++j){
				if(input.getWeight(j) < i->at(j).lower || input.getWeight(j) > i->at(j).upper){
					flag = false;
					continue;
				}
			}
			if(flag){
				curEps = this->epsilons.at(curInt);
			}
			++curInt;
		}

		if(maxObjVal < curEps){
			return;
		}
	}
	(this->archive).push_back(input);
	return;
}

void Population::update(int level){
	std::vector<std::list<Chromosome>::const_iterator> filter = this->filtering1();

	double utility = -99999999;
	std::list<Chromosome>::const_iterator cand = *(filter.begin());
	std::vector<double> max(NOBJ, -9999999);	//debug
	std::vector<double> min(NOBJ, 99999999);	//debug
	for(std::vector<std::list<Chromosome>::const_iterator>::iterator i = filter.begin(); i != filter.end(); ++i){
		double curUtility = 99999999;
		for(int j = 0; j != NOBJ; ++j){
			int convert = 1;
			if(OBJT.at(j) == false){
				convert = -1;
			}
			double temp = COEF.at(j) * (*i)->getObj(j) * convert;
			if((*i)->getObj(j) < min.at(j)){
				min.at(j) = (*i)->getObj(j);
			}
			if((*i)->getObj(j) > max.at(j)){
				max.at(j) = (*i)->getObj(j);
			}
			if(temp < curUtility){
				curUtility = temp;
			}
		}
		if(curUtility > utility){
			utility = curUtility;
			cand = (*i);
		}
	}

	std::cout << "Filtered solutions:" << std::endl;
	for(std::vector<std::list<Chromosome>::const_iterator>::iterator i = filter.begin(); i != filter.end(); ++i){
		for(int j = 0; j != NOBJ; ++j){
			std::cout << (*i)->getObj(j) << " ";
		}
		std::cout << std::endl;
	}
	std::cout << std::endl;

	std::cout << "Selected solution:" << std::endl;
	for(int j = 0; j != NOBJ; ++j){
		std::cout << cand->getObj(j) << " ";
	}
	std::cout << std::endl << std::endl;

	std::cout << "Max values:" << std::endl;
	for(std::vector<double>::iterator i = max.begin(); i != max.end(); ++i){
		std::cout << *i << "\t";
	}
	std::cout << std::endl;
	std::cout << "Min values:" << std::endl;
	for(std::vector<double>::iterator i = min.begin(); i != min.end(); ++i){
		std::cout << *i << "\t";
	}
	std::cout << std::endl;

	std::cout << "Weights:" << std::endl;																			//debug
	for(int i = 0; i != NOBJ; ++i){																					//debug
		std::cout << cand->getWeight(i) << "\t";																	//debug
	}																												//debug
	std::cout << std::endl;																							//debug

	std::vector<couple> newInterval;
	for(int i = 0; i != NOBJ; ++i){
		double low = cand->getWeight(i) - std::pow(R, level) / 2.0;
		double up = cand->getWeight(i) + std::pow(R, level) / 2.0;
		if(low <= 0.0){
			couple temp(0.0, std::pow(R, level));
			newInterval.push_back(temp);
		}
		else if(up >= 1.0){
			couple temp(1 - std::pow(R, level), 1.0);
			newInterval.push_back(temp);
		}
		else{
			couple temp(low, up);
			newInterval.push_back(temp);
		}
	}
	this->intervals.push_back(newInterval);

	this->epsilons.push_back(*(this->epsilons.end() - 1) / std::exp(EPSMULTIPLIER));
//	this->epsilons.push_back(*(this->epsilons.end() - 1) - EPSMULTIPLIER);


	std::cout << "Epsilons:" << std::endl;																			//debug
	for(std::vector<double>::iterator i = this->epsilons.begin(); i != this->epsilons.end(); ++i){					//debug
		std::cout << *i << "\t";																					//debug
	}																												//debug
	std::cout << std::endl;																							//debug
	std::cout << "Intervals:" << std::endl;																			//debug
	for(std::vector<std::vector<couple> >::iterator i = this->intervals.begin(); i != this->intervals.end(); ++i){	//debug
		for(std::vector<couple>::iterator j = i->begin(); j != i->end(); ++j){										//debug
			std::cout << "(" << j->lower << "," << j->upper << ")\t";												//debug
		}																											//debug
		std::cout << std::endl;																						//debug
	}																												//debug
	std::cout << std::endl;																							//debug


	return;
}

std::vector<std::list<Chromosome>::const_iterator> Population::filtering() const{
	std::vector<std::list<Chromosome>::const_iterator> filter1;
	std::vector<couple> curInterval = *(this->intervals.end() - 1);


	//Filter the solutions that fall into the last used interval
	for(std::list<Chromosome>::const_iterator i = this->archive.begin(); i != this->archive.end(); ++i){
		bool flag = true;
		for(int j = 0; j != NOBJ; ++j){
			if(i->getWeight(j) < curInterval.at(j).lower || i->getWeight(j) > curInterval.at(j).upper){
				flag = false;
				break;
			}
		}
		if(flag){
			filter1.push_back(i);
		}
	}

	//Identify solutions that are eps-dominated by another solution AND cannot eps-dominate that solution
	double curEps = *(this->epsilons.end() - 1);
	std::vector<bool> domIndex(filter1.size(), false);
	int count1 = 0;
	for(std::vector<std::list<Chromosome>::const_iterator>::iterator i = filter1.begin(); i != filter1.end(); ++i){
		if(domIndex.at(count1) == true){
			continue;
		}
		int count2 = count1 + 1;
		for(std::vector<std::list<Chromosome>::const_iterator>::iterator j = i + 1; j != filter1.end(); ++j){
			if(domIndex.at(count2) == true){
				continue;
			}
			bool result1 = (*i)->checkDomEps(**j, curEps);
			bool result2 = (*j)->checkDomEps(**i, curEps);
			if(result1 == true && result2 != true){
				domIndex.at(count2) = true;
			}
			if(result1 != true && result2 == true){
				domIndex.at(count1) = true;
			}
			++count2;
		}
		++count1;
	}
	

	//Remove the identified solutions
	std::cout << "Size: " << filter1.size() << std::endl;

	std::vector<std::list<Chromosome>::const_iterator>::iterator iter = filter1.begin();
	for(std::vector<bool>::iterator i = domIndex.begin(); i != domIndex.end(); ++i){
		if(*i == true){
			iter = filter1.erase(iter);
		}
		else {
			++iter;
		}
	}

	std::cout << "Size: " << filter1.size() << std::endl;

	
	//Calculate rectilinear distances
	std::vector<std::vector<double> > distances(filter1.size(), std::vector<double>(filter1.size(), 0.0));
	count1 = 0;
	for(std::vector<std::list<Chromosome>::const_iterator>::iterator i = filter1.begin(); i != filter1.end(); ++i){
		int count2 = count1 + 1;
		for(std::vector<std::list<Chromosome>::const_iterator>::iterator j = i + 1; j != filter1.end(); ++j){
			double distance = 0.0;
			for(int k = 0; k != NOBJ; ++k){
				distance += std::fabs((*i)->getSObj(k) - (*j)->getSObj(k));
			}
			distances.at(count1).at(count2) = distance;
			distances.at(count2).at(count1) = distance;
			++count2;
		}
		++count1;
	}

	//Begin next filtering phase

	std::vector<int> filter2;
	std::vector<std::list<Chromosome>::const_iterator> out;

	//Set number of solutions to be filtered to 2*P if this is the first iteration. Otherwise, P.
	int number = P;
	if(this->epsilons.size() == 1){
		number = 2 * P;
	}	

	//Start with the two most distant solutions
	count1 = 0;
	int count2 = 0;
	double maxDist = 0;
	for(int i = 0; i != filter1.size(); ++i){
		for(int j = i + 1; j != filter1.size(); ++j){
			if(distances.at(i).at(j) > maxDist){
				maxDist = distances.at(i).at(j);
				count1 = i;
				count2 = j;
			}
		}
	}

	filter2.push_back(count1);
	filter2.push_back(count2);
	out.push_back(filter1.at(count1));
	out.push_back(filter1.at(count2));
	
	//Fill up until filter has NOBJ solutions, each time selecting the most distant to all solutions in the filter
	while(filter2.size() != NOBJ){
		double distance = 0.0;
		int curItem = 0;
		for(int i = 0; i != filter1.size(); ++i){
			double tempDist = 0.0;
			for(std::vector<int>::iterator j = filter2.begin(); j != filter2.end(); ++j){
				tempDist += distances.at(i).at(*j);
			}
			if(tempDist > distance){
				std::vector<int>::iterator dup = std::find(filter2.begin(), filter2.end(), i);
				if(dup == filter2.end()){
					distance = tempDist;
					curItem = i;
				}
			}
		}
		filter2.push_back(curItem);
		out.push_back(filter1.at(curItem));
	}

	//Add the most close solution to all solutions in the filter
	double distance = 9999999.0;
	int curItem = 0;
	for(int i = 0; i != filter1.size(); ++i){
		double tempDist = 0.0;
		for(std::vector<int>::iterator j = filter2.begin(); j != filter2.end(); ++j){
			tempDist += distances.at(i).at(*j);
		}
		if(tempDist < distance){
			std::vector<int>::iterator dup = std::find(filter2.begin(), filter2.end(), i);
			if(dup == filter2.end()){
				distance = tempDist;
				curItem = i;
			}
		}
	}
	filter2.push_back(curItem);
	out.push_back(filter1.at(curItem));

	
	//Fill the rest, each time selecting the most distant to all solutions in the filter
	while(filter2.size() != number){
		double distance = 0.0;
		int curItem = 0;
		for(int i = 0; i != filter1.size(); ++i){
			double tempDist = 0.0;
			for(std::vector<int>::iterator j = filter2.begin(); j != filter2.end(); ++j){
				tempDist += distances.at(i).at(*j);
			}
			if(tempDist > distance){
				std::vector<int>::iterator dup = std::find(filter2.begin(), filter2.end(), i);
				if(dup == filter2.end()){
					distance = tempDist;
					curItem = i;
				}
			}
		}
		filter2.push_back(curItem);
		out.push_back(filter1.at(curItem));
	}
	
	return out;
}

Chromosome Population::getBest() const{
	double utility = -99999999;
	std::list<Chromosome>::const_iterator cand;
	for(std::list<Chromosome>::const_iterator i = this->archive.begin(); i != this->archive.end(); ++i){
		double curUtility = 99999999;
		for(int j = 0; j != NOBJ; ++j){
			int convert = 1;
			if(OBJT.at(j) == false){
				convert = -1;
			}
			double temp = COEF.at(j) * i->getObj(j) * convert;
			if(temp < curUtility){
				curUtility = temp;
			}
		}
		if(curUtility > utility){
			utility = curUtility;
			cand = i;
		}
	}
	std::cout << std::endl << "Utility: " << utility << std::endl;

	std::ofstream bestu("BUTILITY");
	bestu << utility;

	std::ofstream lastInterval("LASTINT");
	for(int i = 0; i != NOBJ; ++i){
		lastInterval << (this->intervals.end() - 1)->at(i).lower << " " << (this->intervals.end() - 1)->at(i).upper << std::endl;
	}

	bestu.close();
	lastInterval.close();

	return *cand;
}

std::vector<std::list<Chromosome>::const_iterator> Population::filtering1() const{
	std::vector<std::list<Chromosome>::const_iterator> filter1;
	std::vector<couple> curInterval = *(this->intervals.end() - 1);


	//Filter the solutions that fall into the last used interval
	for(std::list<Chromosome>::const_iterator i = this->archive.begin(); i != this->archive.end(); ++i){
		bool flag = true;
		for(int j = 0; j != NOBJ; ++j){
			if(i->getWeight(j) < curInterval.at(j).lower || i->getWeight(j) > curInterval.at(j).upper){
				flag = false;
				break;
			}
		}
		if(flag){
			filter1.push_back(i);
		}
	}

	//Identify solutions that are eps-dominated by another solution AND cannot eps-dominate that solution
	double curEps = *(this->epsilons.end() - 1);
	std::vector<bool> domIndex(filter1.size(), false);
	int count1 = 0;
	for(std::vector<std::list<Chromosome>::const_iterator>::iterator i = filter1.begin(); i != filter1.end(); ++i){
		if(domIndex.at(count1) == true){
			continue;
		}
		int count2 = count1 + 1;
		for(std::vector<std::list<Chromosome>::const_iterator>::iterator j = i + 1; j != filter1.end(); ++j){
			if(domIndex.at(count2) == true){
				continue;
			}
			bool result1 = (*i)->checkDomEps(**j, curEps);
			bool result2 = (*j)->checkDomEps(**i, curEps);
			if(result1 == true && result2 != true){
				domIndex.at(count2) = true;
			}
			if(result1 != true && result2 == true){
				domIndex.at(count1) = true;
			}
			++count2;
		}
		++count1;
	}
	

	//Remove the identified solutions
	std::vector<std::list<Chromosome>::const_iterator>::iterator iter = filter1.begin();
	for(std::vector<bool>::iterator i = domIndex.begin(); i != domIndex.end(); ++i){
		if(*i == true){
			iter = filter1.erase(iter);
		}
		else {
			++iter;
		}
	}
	
	//Calculate rectilinear distances
	std::vector<std::vector<double> > distances(filter1.size(), std::vector<double>(filter1.size(), 0.0));
	count1 = 0;
	for(std::vector<std::list<Chromosome>::const_iterator>::iterator i = filter1.begin(); i != filter1.end(); ++i){
		int count2 = count1 + 1;
		for(std::vector<std::list<Chromosome>::const_iterator>::iterator j = i + 1; j != filter1.end(); ++j){
			double distance = 0.0;
			for(int k = 0; k != NOBJ; ++k){
				distance += std::fabs((*i)->getSObj(k) - (*j)->getSObj(k));
			}
			distances.at(count1).at(count2) = distance;
			distances.at(count2).at(count1) = distance;
			++count2;
		}
		++count1;
	}

	//Begin next filtering phase

	std::vector<int> filter2;
	std::vector<std::list<Chromosome>::const_iterator> out;

	//Set number of solutions to be filtered to 2*P if this is the first iteration or we are presenting the final solutions. Otherwise, P. 
	int number = P;
	if(this->epsilons.size() == 1 || this->epsilons.size() >= LEVELS){
		number = 2 * P;
	}	

	//Start with the two most distant solutions
	count1 = 0;
	int count2 = 0;
	double maxDist = 0;
	for(int i = 0; i != filter1.size(); ++i){
		for(int j = i + 1; j != filter1.size(); ++j){
			if(distances.at(i).at(j) > maxDist){
				maxDist = distances.at(i).at(j);
				count1 = i;
				count2 = j;
			}
		}
	}

	filter2.push_back(count1);
	filter2.push_back(count2);
	out.push_back(filter1.at(count1));
	out.push_back(filter1.at(count2));
	
	//Fill the rest, each time selecting the most distant to all solutions in the filter
	while(filter2.size() != number){
		double distance = 0.0;
		int curItem = 0;
		for(int i = 0; i != filter1.size(); ++i){
			double tempDist = 999999999;
			for(std::vector<int>::iterator j = filter2.begin(); j != filter2.end(); ++j){
				if(distances.at(i).at(*j) < tempDist){
					tempDist = distances.at(i).at(*j);
				}
			}
			if(tempDist > distance){
				std::vector<int>::iterator dup = std::find(filter2.begin(), filter2.end(), i);
				if(dup == filter2.end()){
					distance = tempDist;
					curItem = i;
				}
			}
		}
		filter2.push_back(curItem);
		out.push_back(filter1.at(curItem));
	}
	
	return out;
}

Chromosome Population::getFilteredBest() const{
	std::vector<std::list<Chromosome>::const_iterator> filter = this->filtering1();

	double utility = -99999999;
	std::list<Chromosome>::const_iterator cand = *(filter.begin());
	std::vector<double> max(NOBJ, -9999999);	//debug
	std::vector<double> min(NOBJ, 99999999);	//debug
	for(std::vector<std::list<Chromosome>::const_iterator>::iterator i = filter.begin(); i != filter.end(); ++i){
		double curUtility = 99999999;
		for(int j = 0; j != NOBJ; ++j){
			int convert = 1;
			if(OBJT.at(j) == false){
				convert = -1;
			}
			double temp = COEF.at(j) * (*i)->getObj(j) * convert;
			if((*i)->getObj(j) < min.at(j)){
				min.at(j) = (*i)->getObj(j);
			}
			if((*i)->getObj(j) > max.at(j)){
				max.at(j) = (*i)->getObj(j);
			}
			if(temp < curUtility){
				curUtility = temp;
			}
		}
		if(curUtility > utility){
			utility = curUtility;
			cand = (*i);
		}
	}

	std::cout << std::endl << "Utility: " << utility << std::endl;

	std::ofstream bestu("BUTILITYF");
	bestu << utility;

	bestu.close();

	return *cand;
}