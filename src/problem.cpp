#include "problem.h"

/**************************DEFINITIONS**************************/
int SEED = 10;
CRandomMersenne rg(SEED);
int problemNo = 1;
int NOBJ = 0;
int NCONT = 0;
int NINT = 0;
std::vector<double> MAXC;
std::vector<double> MINC;
std::vector<int> MAXI;
std::vector<int> MINI;
std::vector<bool> OBJT;
std::vector<double> IDEAL;
std::vector<double> NADIR;
std::vector<double> LAMBDA;

std::vector<double> COEF;
std::vector<couple> LIMITS;

double W = 0;
double R = 0;
int P = 0;
int LEVELS = 0;

std::vector<std::vector<double> > D;

int etaC = 20;
int etaM = 20;
double mProb = 0.15;
double mProbBin = 0.15;
double cProbBin = 0.50;

double PI = 3.14159265;


void setProblem(int problemNo1){
	switch(problemNo1){
		case 1:			//ZDT1
			NOBJ = 2;
			NCONT = 30;
			NINT = 0;
			MAXC = std::vector<double>(NCONT, 1);
			MINC = std::vector<double>(NCONT, 0);
			MAXI.clear();
			MINI.clear();
			OBJT = std::vector<bool>(NOBJ, false);
			for(int i = 0; i != NOBJ; ++i){
				IDEAL.push_back(0);
				NADIR.push_back(1);
				LAMBDA.push_back(0.3);
			}
			break;

		case 2:
			NOBJ = 2;
			NCONT = 30;
			NINT = 0;
			MAXC = std::vector<double>(NCONT, 1);
			MINC = std::vector<double>(NCONT, 0);
			MAXI.clear();
			MINI.clear();
			OBJT = std::vector<bool>(NOBJ, false);
			for(int i = 0; i != NOBJ; ++i){
				IDEAL.push_back(0);
				NADIR.push_back(1);
				LAMBDA.push_back(0.3);
			}
			break;

		case 3:
			NOBJ = 2;
			NCONT = 30;
			NINT = 0;
			MAXC = std::vector<double>(NCONT, 1);
			MINC = std::vector<double>(NCONT, 0);
			MAXI.clear();
			MINI.clear();
			OBJT = std::vector<bool>(NOBJ, false);
			IDEAL.push_back(0);
			IDEAL.push_back(-0.773);
			NADIR.push_back(0.852);
			NADIR.push_back(1);
			for(int i = 0; i != NOBJ; ++i){
				LAMBDA.push_back(0.3);
			}
			break;

		case 4:
			NOBJ = 2;
			NCONT = 10;
			NINT = 0;
			MAXC.push_back(1);
			MINC.push_back(0);
			MAXI.clear();
			MINI.clear();
			OBJT = std::vector<bool>(NOBJ, false);
			for(int i = 1; i != NCONT; ++i){
				MAXC.push_back(5);
				MINC.push_back(-5);
			}
			for(int i = 0; i != NOBJ; ++i){
				IDEAL.push_back(0);
				NADIR.push_back(1);
				LAMBDA.push_back(0.3);
			}
			break;

		case 5:			//ZDT5
			NOBJ = 2;
			NCONT = 0;
			NINT = 11;
			MAXC.clear();
			MINC.clear();
			MAXI.clear();
			MINI.clear();
			MAXI.push_back(30);
			MINI.push_back(0);
			for(int i = 1; i != NINT; ++i){
				MAXI.push_back(6);
				MINI.push_back(1);
			}
			OBJT = std::vector<bool>(NOBJ, false);
			IDEAL.push_back(1);
			IDEAL.push_back(0.3);
			NADIR.push_back(31);
			NADIR.push_back(10);
			LAMBDA.push_back(10);
			LAMBDA.push_back(3);
			break;

		case 6:			//ZDT6
			NOBJ = 2;
			NCONT = 10;
			NINT = 0;
			MAXC.assign(10, 1);
			MINC.assign(10, 0);
			MAXI.clear();
			MINI.clear();
			OBJT = std::vector<bool>(NOBJ, false);
			IDEAL.push_back(0.2);
			IDEAL.push_back(0);
			NADIR.push_back(1);
			NADIR.push_back(0.95);
			LAMBDA.push_back(0.3);
			LAMBDA.push_back(0.3);
			break;

		case 7:			//DTLZ1
			NOBJ = 3;
			NCONT = 7;
			NINT = 0;
			MAXC.assign(NCONT, 1);
			MINC.assign(NCONT, 0);
			MAXI.clear();
			MINI.clear();
			OBJT = std::vector<bool>(NOBJ, false);
			for(int i = 0; i != NOBJ; ++i){
				IDEAL.push_back(0);
				NADIR.push_back(0.5);
				LAMBDA.push_back(0.15);
			}
			break;

		case 8:			//DTLZ2
			NOBJ = 3;
			NCONT = 12;
			NINT = 0;
			MAXC.assign(NCONT, 1);
			MINC.assign(NCONT, 0);
			MAXI.clear();
			MINI.clear();
			OBJT = std::vector<bool>(NOBJ, false);
			for(int i = 0; i != NOBJ; ++i){
				IDEAL.push_back(0);
				NADIR.push_back(1);
				LAMBDA.push_back(0.3);
			}
			break;

		case 9:			//DTLZ3
			NOBJ = 3;
			NCONT = 12;
			NINT = 0;
			MAXC.assign(NCONT, 1);
			MINC.assign(NCONT, 0);
			MAXI.clear();
			MINI.clear();
			OBJT = std::vector<bool>(NOBJ, false);
			for(int i = 0; i != NOBJ; ++i){
				IDEAL.push_back(0);
				NADIR.push_back(1);
				LAMBDA.push_back(0.3);
			}
			break;

		case 10:		//DTLZ4
			NOBJ = 3;
			NCONT = 12;
			NINT = 0;
			MAXC.assign(NCONT, 1);
			MINC.assign(NCONT, 0);
			MAXI.clear();
			MINI.clear();
			OBJT = std::vector<bool>(NOBJ, false);
			for(int i = 0; i != NOBJ; ++i){
				IDEAL.push_back(0);
				NADIR.push_back(1);
				LAMBDA.push_back(0.3);
			}
			break;

		case 11:		//DTLZ5
			NOBJ = 3;
			NCONT = 12;
			NINT = 0;
			MAXC.assign(NCONT, 1);
			MINC.assign(NCONT, 0);
			MAXI.clear();
			MINI.clear();
			OBJT = std::vector<bool>(NOBJ, false);
			for(int i = 0; i != NOBJ; ++i){
				IDEAL.push_back(0);
				NADIR.push_back(1);
				LAMBDA.push_back(0.3);
			}
			break;

		case 12:		//DTLZ6
			NOBJ = 3;
			NCONT = 12;
			NINT = 0;
			MAXC.assign(NCONT, 1);
			MINC.assign(NCONT, 0);
			MAXI.clear();
			MINI.clear();
			OBJT = std::vector<bool>(NOBJ, false);
			for(int i = 0; i != NOBJ; ++i){
				IDEAL.push_back(0);
				NADIR.push_back(1);
				LAMBDA.push_back(0.3);
			}
			break;

		case 13:		//DTLZ7
			NOBJ = 3;
			NCONT = 22;
			NINT = 0;
			MAXC.assign(NCONT, 1);
			MINC.assign(NCONT, 0);
			MAXI.clear();
			MINI.clear();
			OBJT = std::vector<bool>(NOBJ, false);
			IDEAL.push_back(0);
			IDEAL.push_back(0);
			IDEAL.push_back(4);
			NADIR.push_back(1);
			NADIR.push_back(1);
			NADIR.push_back(6);
			LAMBDA.push_back(0.3);
			LAMBDA.push_back(0.3);
			LAMBDA.push_back(0.55);
			break;

		case 14:		//L2-ZDT4
			NOBJ = 2;
			NCONT = 10;
			NINT = 0;
			MAXC.push_back(1);
			MINC.push_back(0);
			MAXI.clear();
			MINI.clear();
			OBJT = std::vector<bool>(NOBJ, false);
			for(int i = 1; i != NCONT; ++i){
				MAXC.push_back(5);
				MINC.push_back(-5);
			}
			for(int i = 0; i != NOBJ; ++i){
				IDEAL.push_back(0);
				NADIR.push_back(1);
				LAMBDA.push_back(0.3);
			}
			for(int i = 1; i != NCONT; ++i){
				std::vector<double> temp;
				for(int j = 1; j != NCONT; ++j){
					temp.push_back(-1 + rg.Random() * 2);
				}
				D.push_back(temp);
			}
			break;
	}

	mProb = 1.0 /NCONT;		//setting mutation probability as Deb's recommendation

	return;
}

std::vector<double> calcObj(std::vector<double> cVar, std::vector<int> iVar){
	std::vector<double> obj(NOBJ, 0);
	double f1 = 0;
	double g = 0;
	double h = 0;
	int n = NCONT;
    int k = n - NOBJ + 1;
	double alpha = 100;
	double t = 0;
	std::vector<double> theta;
	switch(problemNo){
		case 1:			//ZDT1
			f1 = cVar.at(0);
			g = 0.0;
			for(int i = 1; i != NCONT; ++i){
				g += cVar.at(i);
			}
			g = 1.0 + ((9.0 * g) / 29.0);
			h = 1.0 - std::sqrt(f1 / g);
			obj.at(0) = f1;
			obj.at(1) = g * h;
			break;

		case 2:			//ZDT2
			f1 = cVar.at(0);
			g = 0.0;
			for(int i = 1; i != NCONT; ++i){
				g += cVar.at(i);
			}
			g = 1.0 + (9.0 * g) / (NCONT - 1.0);
			h = 1 - std::pow(f1 / g, 2);
			obj.at(0) = f1;
			obj.at(1) = g * h;
			break;

		case 3:			//ZDT3
			f1 = cVar.at(0);
			g = 0.0;
			for(int i = 1; i != NCONT; ++i){
				g += cVar.at(i);
			}
			g = 1.0 + (9.0 * g) / (NCONT - 1.0);
			h = 1 - std::sqrt(f1 / g) - (f1 / g) * std::sin(10 * PI * f1);
			obj.at(0) = f1;
			obj.at(1) = g * h;
			break;

		case 4:			//ZDT4
			f1 = cVar.at(0);
			g = 91.0;
			for(int i = 1; i != NCONT; ++i){
				g += pow(cVar.at(i), 2) - 10.0 * std::cos(4.0 * PI * cVar.at(i));
			}
			h = 1.0 - std::sqrt(f1 / g);
			obj.at(0) = f1;
			obj.at(1) = g * h;
			break;

		case 5:			//ZDT5
			f1 = 1 + *(iVar.begin());
			g = 0.0;
			for(int i = 1; i != NINT; ++i){
				g+= iVar.at(i);
			}
			h = 1.0 / f1;
			obj.at(0) = f1;
			obj.at(1) = g * h;
			break;

		case 6:			//ZDT6
			f1 = 1.0 - std::exp(-4 * cVar.at(0)) * std::pow(std::sin(6 * PI * cVar.at(0)), 6);
			g = 0.0;
			for(int i = 1; i != NCONT; ++i){
				g += cVar.at(i);
			}
			g = 1.0 + 9 * std::pow(g / 9.0, 0.25);
			h = 1.0 - std::pow(f1 / g, 2);
			obj.at(0) = f1;
			obj.at(1) = g * h;
			break;

		case 7:			//DTLZ1
			g = 0;
			for (int i = n - k + 1; i <= n; i++){
				g += pow(cVar.at(i-1)-0.5,2) - cos(20 * PI * (cVar.at(i-1)-0.5));
			}
			g = 100 * (k + g);
			for (int i = 1; i <= NOBJ; i++){
				double f = 0.5 * (1 + g);
				for (int j = NOBJ - i; j >= 1; j--){
					f *= cVar.at(j-1);
				}
				if (i > 1){
					f *= 1 - cVar.at((NOBJ - i + 1) - 1);
				}
				obj.at(i-1) = f;
			}
			break;

		case 8:			//DTLZ2
			g = 0;
			for (int i = n - k + 1; i <= n; i++){
				g += pow(cVar.at(i-1)-0.5,2);
			}
			for (int i = 1; i <= NOBJ; i++){
				double f = (1 + g);
				for (int j = NOBJ - i; j >= 1; j--){
					f *= cos(cVar.at(j-1) * PI / 2);
				}
				if (i > 1){
					f *= sin(cVar.at((NOBJ - i + 1) - 1) * PI / 2);
				}
				obj.at(i-1) = f;
			}
			break;

		case 9:			//DTLZ3
			g = 0;
			for (int i = n - k + 1; i <= n; i++){
				g += pow(cVar.at(i-1)-0.5,2) - cos(20 * PI * (cVar.at(i-1)-0.5));
			}
			g = 100 * (k + g);

			for (int i = 1; i <= NOBJ; i++){
				double f = (1 + g);
				for (int j = NOBJ - i; j >= 1; j--){
					f *= cos(cVar.at(j-1) * PI / 2);
				}
				if (i > 1){
					f *= sin(cVar.at((NOBJ - i + 1) - 1) * PI / 2);
				}
				obj.at(i-1) = f;
			}
			break;

		case 10:			//DTLZ4
			alpha = 100;
			g = 0;
			for (int i = n - k + 1; i <= n; i++){
				g += pow(cVar.at(i-1)-0.5,2);
			}
			for (int i = 1; i <= NOBJ; i++){
				double f = (1 + g);
				for (int j = NOBJ - i; j >= 1; j--)
				{
					f *= cos(pow(cVar.at(j-1),alpha) * PI / 2);
				}
				if (i > 1)
				{
					f *= sin(pow(cVar.at((NOBJ - i + 1) - 1),alpha) * PI / 2);
				}

				obj.at(i-1) = f;
			}
			break;

		case 11:			//DTLZ5
			theta.assign(NOBJ, 0);
			t = 0;
			g = 0;

			for (int i = n - k + 1; i <= n; i++)
			{
				g += pow(cVar.at(i-1) - 0.5, 2);
			}

			t = PI / (4 * (1 + g));
			theta.at(0) = cVar.at(0) * PI / 2;
			for (int i = 2; i <= NOBJ - 1; i++)
			{
				theta.at(i-1) = t * (1 + 2 * g * cVar.at(i-1));
			}

			for (int i = 1; i <= NOBJ; i++)
			{
				double f = (1 + g);
				for (int j = NOBJ - i; j >= 1; j--)
				{
					f *= cos(theta.at(j-1));
				}
				if (i > 1)
				{
					f *= sin(theta.at((NOBJ - i + 1) - 1));
				}

				obj.at(i-1) = f;
			}
			break;

		case 12:			//DTLZ6
			theta.assign(NOBJ, 0);
			t = 0;
			g = 0;

			for (int i = n - k + 1; i <= n; i++)
			{
				g += pow(cVar.at(i-1), 0.1);
			}

			t = PI / (4 * (1 + g));
			theta.at(0) = cVar.at(0) * PI / 2;
			for (int i = 2; i <= NOBJ - 1; i++)
			{
				theta.at(i-1) = t * (1 + 2 * g * cVar.at(i-1));
			}

			for (int i = 1; i <= NOBJ; i++)
			{
				double f = (1 + g);
				for (int j = NOBJ - i; j >= 1; j--)
				{
					f *= cos(theta.at(j-1));
				}
				if (i > 1)
				{
					f *= sin(theta.at((NOBJ - i + 1) - 1));
				}

				obj.at(i-1) = f;
			}
			break;

		case 13:			//DTLZ7
			g = 0;
			h = 0;
			for (int i = n - k + 1; i <= n; i++){
				g += cVar.at(i-1);
			}
			g = 1 + 9 * g / k;
			for (int i = 1; i <= NOBJ - 1; i++){
				obj.at(i-1) = cVar.at(i-1);
			}
			for (int j = 1; j <= NOBJ - 1; j++){
				h += cVar.at(j-1) / (1 + g) * (1 + sin(3 * PI * cVar.at(j-1)));
			}
			h = NOBJ - h;
			obj.at(NOBJ - 1) = (1 + g) * h;
			break;

		case 14:			//L2-ZDT4
			std::vector<double> y = cVar;
			for(int i = 0; i != NCONT - 1; ++i){
				double temp = 0;
				for(int j = 0; j != NCONT - 1; ++j){
					temp += cVar.at(j + 1) * D.at(i).at(j);
				}
				y.at(i + 1) = temp;
			}
			f1 = y.at(0);
			g = 91.0;
			for(int i = 1; i != NCONT; ++i){
				g += pow(y.at(i), 2) - 10.0 * std::cos(4.0 * PI * y.at(i));
			}
			h = 1.0 - std::sqrt(f1 / g);
			obj.at(0) = f1;
			obj.at(1) = g * h;
			break;
	}
	return obj;
}

void setLimits(){
	for(int i = 0; i != NOBJ; ++i){
		couple temp(0.0, 1.0);
		std::cout << "Enter lower limit for objective " << i + 1 << " : ";
		std::cin >> temp.lower;
		std::cout << "Enter upper limit for objective " << i + 1 << " : ";
		std::cin >> temp.upper;
		LIMITS.push_back(temp);
	}
	return;
}

void setCoef(){
	for(int i = 0; i != NOBJ; ++i){
		double in = 0;
		std::cout << "Utility function coefficient for objective " << i + 1 << ": ";
		std::cin >> in;
		COEF.push_back(in);
	}
	return;
}

void setParams(int levels){
	LEVELS = levels;
	P = 2 * NOBJ;
	W = 2.0 / (2.0 * NOBJ);
	R = std::pow(1.0 / P, 1.0 / NOBJ) + std::pow(W, LEVELS - 1);
	return;
}

