#include "chromosome.h"

Chromosome::Chromosome(): obj(NOBJ,0), sobj(NOBJ,0){	//constructor
	for(int i = 0; i != NCONT; ++i){
		double x = MINC.at(i) + rg.Random() * (MAXC.at(i) - MINC.at(i));
		(this->cVar).push_back(x);
	}
	for(int i = 0; i != NINT; ++i){
		int x = MINI.at(i) + static_cast<int>(rg.Random() * (MAXI.at(i) - MINI.at(i) + 1));
		(this->iVar).push_back(x);
	}
	this->calculate();
	return;
}

Chromosome::Chromosome(std::vector<double> inCont, std::vector<int> inInt): obj(NOBJ,0), sobj(NOBJ,0){
	this->cVar = inCont;
	this->iVar = inInt;
	this->calculate();
}

int Chromosome::checkDom(const Chromosome& second) const{
	int count1 = 0;
	int count2 = 0;
	for(int i = 0; i != NOBJ; ++i){
		if(OBJT.at(i) == 0){
			if((this->obj).at(i) < second.obj.at(i)){
				++count1;
			}
			else if((this->obj).at(i) > second.obj.at(i)){
				++count2;
			}
		}
		else{
			if((this->obj).at(i) > second.obj.at(i)){
				++count1;
			}
			else if((this->obj).at(i) < second.obj.at(i)){
				++count2;
			}
		}
	}
	if(count1 > 0 && count2 == 0){
		return 1;
	}
	else if(count1 == 0 && count2 > 0){
		return 2;
	}
	else{
		return 0;
	}
}

void Chromosome::scale(){
	for(int i = 0; i != NOBJ; ++i){
		if(OBJT.at(i) == false){
			double shift = IDEAL.at(i) - 0.0;
			if((this->obj).at(i) <= NADIR.at(i)){
				(this->sobj).at(i) = (sigma(NADIR.at(i) - shift, i) / (NADIR.at(i) - shift)) * ((this->obj).at(i) - shift);
			}
			else{
				(this->sobj).at(i) = sigma((this->obj).at(i) - shift, i);
			}
		}
		else{
			double shift = IDEAL.at(i) - 0.0;
			if((this->obj).at(i) >= NADIR.at(i)){
				(this->sobj).at(i) = 1.0 - (sigma(-(NADIR.at(i) - shift), i) / (-(NADIR.at(i) - shift))) * (-((this->obj).at(i) - shift));
			}
			else{
				(this->sobj).at(i) = 1.0 - sigma(-((this->obj).at(i) - shift), i);
			}
		}
	}
	return;
}


void Chromosome::SBX(const Chromosome & second, std::vector<double> &cont1, std::vector<double> &cont2) const{
	for(int i = 0; i != NCONT; ++i){
		if(rg.Random() <= 0.5) {	//crossover each variable with prob. 0.5
			double x1 = 0;
			double x2 = 0;
			if((this->cVar).at(i) < second.cVar.at(i)){
				x1 = (this->cVar).at(i);
				x2 = second.cVar.at(i);
			}
			else if((this->cVar).at(i) > second.cVar.at(i)){
				x2 = (this->cVar).at(i);
				x1 = second.cVar.at(i);
			}
			else {
				cont1.push_back((this->cVar).at(i));
				cont2.push_back(second.cVar.at(i));
				continue;
			}

			double beta = 1.0 + 2.0 * ((x1 - MINC.at(i)) / (x2 - x1));
			double alpha = 2.0 - pow(beta, -(etaC + 1.0));
			double betaq = 0;
			double rand = rg.Random();
			if (rand <= (1.0 / alpha)){
				betaq = pow((rand*alpha),(1.0 / (etaC + 1.0)));
			}
			else{
				betaq = pow((1.0 / (2.0 - rand*alpha)),(1.0 / (etaC + 1.0)));
			}
			double c1 = 0.5 * ((x1+x2)-betaq*(x2-x1));

			beta = 1.0 + 2.0 * ((MAXC.at(i) - x2) / (x2 - x1));
			alpha = 2.0 - pow(beta, -(etaC + 1.0));
			betaq = 0;
			if (rand <= (1.0 / alpha)){
				betaq = pow((rand*alpha),(1.0 / (etaC + 1.0)));
			}
			else{
				betaq = pow((1.0 / (2.0 - rand*alpha)),(1.0 / (etaC + 1.0)));
			}
			double c2 = 0.5 * ((x1+x2)+betaq*(x2-x1));

			if(c1 < MINC.at(i))	c1 = MINC.at(i);
			if(c2 < MINC.at(i))	c2 = MINC.at(i);
			if(c1 > MAXC.at(i))	c1 = MAXC.at(i);
			if(c2 > MAXC.at(i))	c2 = MAXC.at(i);

			if(rg.Random() <= 0.5){
				cont1.push_back(c1);
				cont2.push_back(c2);
			}
			else{
				cont1.push_back(c2);
				cont2.push_back(c1);
			}
		}
		else {
			cont1.push_back((this->cVar).at(i));
			cont2.push_back(second.cVar.at(i));
		}
	}
	return;
}


void Chromosome::INTX(const Chromosome & second, std::vector<int> &int1, std::vector<int> &int2) const{
	for(int i = 0; i != NINT; ++i){
		if(rg.Random() < cProbBin){
			int range = MAXI.at(i) - MINI.at(i);
			int min = this->iVar.at(i);
			int max = second.iVar.at(i);
			if(min > max){
				int temp = min;
				min = max;
				max = temp;
			}
			int c1 = rg.IRandomX(static_cast<int>(min - range * 0.5), static_cast<int>(max + range * 0.5));
			int c2 = rg.IRandomX(static_cast<int>(min - range * 0.5), static_cast<int>(max + range * 0.5));
			if(c1 < MINI.at(i)) c1 = MINI.at(i);
			if(c2 < MINI.at(i)) c2 = MINI.at(i);
			if(c1 > MAXI.at(i)) c1 = MAXI.at(i);
			if(c2 > MAXI.at(i)) c2 = MAXI.at(i);
			int1.push_back(c1);
			int2.push_back(c2);
		}
		else{
			int1.push_back(this->iVar.at(i));
			int2.push_back(second.iVar.at(i));
		}
	}
	return;
}

void Chromosome::polyMut(std::vector<double> &cVar){
	for(int i = 0; i != NCONT; ++i){
		if(rg.Random() < mProb){
			double x = cVar.at(i);
			double delta1 = (x - MINC.at(i)) / (MAXC.at(i) - MINC.at(i));
			double delta2 = (MAXC.at(i) - x) / (MAXC.at(i) - MINC.at(i));
			double mut_pow = 1.0 / (etaM + 1.0);
			double random = rg.Random();
			double deltaq = 0;

			if(random <= 0.5){
				double xx = 1.0 - delta1;
				double val = 2.0 * random + (1.0 - 2.0 * random) * pow(xx, etaM + 1.0);
				deltaq = pow(val, mut_pow) - 1.0;
			}
			else {
				double xx = 1.0 - delta2;
				double val = 2.0 * (1.0 - random) + 2.0 * (random - 0.5) * pow(xx, etaM + 1.0);
				deltaq = 1.0 - pow(val, mut_pow);
			}
			x += deltaq * (MAXC.at(i) - MINC.at(i));
			if(x < MINC.at(i)){
				x = MINC.at(i);
			}
			if(x > MAXC.at(i)){
				x = MAXC.at(i);
			}
			cVar.at(i) = x;
		}
	}
	return;
}

void Chromosome::intMut(std::vector<int>& input){
	for(int i = 0; i != NINT; ++i){
		if(rg.Random() < mProbBin){
			int range = MAXI.at(i) - MINI.at(i);
			int c1 = rg.IRandomX(static_cast<int>(input.at(i) - range * 0.5), static_cast<int>(input.at(i) + range * 0.5));
			if(c1 > MAXI.at(i)) c1 = MAXI.at(i);
			if(c1 < MINI.at(i)) c1 = MINI.at(i);
			input.at(i) = c1;
		}
	}
	return;
}

void Chromosome::calcWeights(){
	int exists = 0;
	double sum = 0.0;
	this->weights.assign(NOBJ, 0.0);
	for(int i = 0; i != NOBJ; ++i){
		if((this->sobj).at(i) == 1.0 * OBJT.at(i)){
			exists = 1;
			break;
		}
		sum += 1.0 / ((this->sobj).at(i) - 1.0 * OBJT.at(i));
	}
	if(exists == 0){
		for(int i = 0; i != NOBJ; ++i){
			this->weights.at(i) = (1.0 / ((this->sobj).at(i) - 1.0 * OBJT.at(i))) / sum;
		}
	}
	else{
		for(int i = 0; i != NOBJ; ++i){
			if((this->sobj).at(i) == 1.0 * OBJT.at(i)){
				this->weights.at(i) = 1;
			}
			else{
				this->weights.at(i) = 0;
			}
		}
	}
	return;
}

bool Chromosome::checkDomEps(const Chromosome& second, double eps) const{
	int count1 = 0;
	int count2 = 0;
	for(int i = 0; i != NOBJ; ++i){
		if(OBJT.at(i) == 0){
			if((this->obj).at(i) - eps < second.obj.at(i)){
				++count1;
			}
			else if((this->obj).at(i) - eps > second.obj.at(i)){
				++count2;
			}
		}
		else{
			if((this->obj).at(i) + eps > second.obj.at(i)){
				++count1;
			}
			else if((this->obj).at(i) + eps < second.obj.at(i)){
				++count2;
			}
		}
	}
	if(count1 > 0 && count2 == 0){
		return true;
	}
	return false;
}

