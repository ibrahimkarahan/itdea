#include "globals.h"

std::vector<int> encode(int input, int bit){ //ONLY FOR NONNEGATIVE NUMBERS!
	std::vector<int> output(bit, 0);
	if(input == 0){
		return output;
	}
	while (input > 0){
		bool found = false;
		int power = 0;
		while(!found){
			if(static_cast<int>(std::pow(2.0, power)) <= input){
				++power;
			}
			else {
				found = true;
			}
		}
		input -= static_cast<int>(std::pow(2.0, power - 1));
		output.at(bit - power) = 1;
	}
	return output;
}

int decode(const std::vector<int>& input){ //ONLY FOR NONNEGATIVE NUMBERS!
	int output = 0;
	int power = static_cast<int>(input.size()) - 1;
	for(std::vector<int>::const_iterator i = input.begin(); i != input.end(); ++i){
		output += (*i) * static_cast<int>(std::pow(2.0, power));
		--power;
	}
	return output;
}

		