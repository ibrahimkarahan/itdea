#ifndef POPULATION_H
#define POPULATION_H

#include <fstream>
#include <vector>
#include <iostream>
#include <list>
#include <algorithm>
#include <ctime>
#include "chromosome.h"
#include "randomc.h"
#include "globals.h"

class Population {
private:
	std::list<Chromosome> pop;
	std::list<Chromosome> archive;
	std::vector<double> epsilons;
	std::vector<std::vector<couple> > intervals;

	std::vector<std::list<Chromosome>::const_iterator> filtering() const;
	std::vector<std::list<Chromosome>::const_iterator> filtering1() const;

public:
	bool evaluatePop(const Chromosome&);
	void evaluateArc(const Chromosome&);
	const Chromosome& choosePop() const;
	const Chromosome& chooseArc() const;
	void update(int);

	Population(int, double);	//constructor

	inline int getPopSize(){
		return static_cast<int>(this->pop.size());
	}

	inline int getArcSize(){
		return static_cast<int>(this->archive.size());
	}

	inline void printPop(std::ofstream &file) const{
		for(std::list<Chromosome>::const_iterator i = this->pop.begin(); i != this->pop.end(); ++i){
			for(int j = 0; j != NOBJ; ++j){
				file << i->getObj(j) << ",";
			}
			file << std::endl;
		}
	}

	inline void printArc(std::ofstream &file) const{
		for(std::list<Chromosome>::const_iterator i = this->archive.begin(); i != this->archive.end(); ++i){
			for(int j = 0; j != NOBJ; ++j){
				file << i->getObj(j) << " ";
			}
			file << std::endl;
		}
	}

	inline void printArcVar(std::ofstream &file) const{
		for(std::list<Chromosome>::const_iterator i = this->archive.begin(); i != this->archive.end(); ++i){
			for(int j = 0; j != NCONT; ++j){
				file << i->getCVar(j) << " ";
			}
			file << std::endl;
		}
	}

	Chromosome getBest() const;
	Chromosome getFilteredBest() const;


};


#endif //POPULATION_H
