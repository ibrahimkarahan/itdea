#ifndef GLOBAL_H
#define GLOBAL_H

#include <vector>
#include <string>
#include <cmath>
#include "randomc.h"

struct couple {
	double lower;
	double upper;

	couple(double first, double second) {
		lower = first;
		upper = second;
	}
};

extern int SEED;
extern CRandomMersenne rg;

extern int problemNo;
extern int NOBJ;
extern int NCONT;
extern int NINT;
extern std::vector<double> MAXC;
extern std::vector<double> MINC;
extern std::vector<int> MAXI;
extern std::vector<int> MINI;
extern std::vector<bool> OBJT;
extern std::vector<double> IDEAL;
extern std::vector<double> NADIR;
extern std::vector<double> LAMBDA;

extern std::vector<double> COEF;
extern std::vector<couple> LIMITS;
extern double W;
extern double R;
extern int P;
extern int LEVELS;
//extern int CURLEVEL;

extern std::vector<std::vector<double> > D;

extern int etaC;
extern int etaM;
extern double mProb;
extern double mProbBin;
extern double cProbBin;

extern double PI;

inline double sigma(double y, int i) {
	return (1.0 / (1.0 + std::exp(-(y / LAMBDA.at(i)))) - 0.5) * 2.0;
}


#endif
