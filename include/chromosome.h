#ifndef CHROMOSOME_H
#define CHROMOSOME_H

#include <vector>
#include <cmath>
#include "randomc.h"
#include "globals.h"
#include "problem.h"

class Chromosome{
private:
	std::vector<double> cVar;	//Values of continuous variables
	std::vector<int> iVar;		//Values of integer variables
	std::vector<double> obj;	//Objective values
	std::vector<double> sobj;	//Scaled objective values
	std::vector<double> weights;//Tchebycheff weights

	void scale();
	void SBX(const Chromosome&, std::vector<double>&, std::vector<double>&) const;
	void INTX(const Chromosome&, std::vector<int>&, std::vector<int>&) const;
	static void polyMut(std::vector<double>&);
	static void intMut(std::vector<int>&);

public:
	void calcWeights();

	int checkDom(const Chromosome&) const;

	bool checkDomEps(const Chromosome&, double) const;

	inline double getObj(const int no) const{
		return this->obj.at(no);
	}

	inline double getSObj(const int no) const{
		return this->sobj.at(no);
	}

	inline double getCVar(const int no) const{
		return this->cVar.at(no);
	}
	
	inline double getWeight(const int no) const{
		return this->weights.at(no);
	}

	inline std::vector<Chromosome> mate(const Chromosome& second) const{
		std::vector<double> cont1, cont2;
		if(NCONT > 0){
			this->SBX(second, cont1, cont2);
			polyMut(cont1);
			polyMut(cont2);
		}
		std::vector<int> int1, int2;
		if(NINT > 0){
			this->INTX(second, int1, int2);
			intMut(int1);
			intMut(int2);
		}
		std::vector<Chromosome> ret;
		Chromosome in1(cont1, int1);
		Chromosome in2(cont2, int2);
		ret.push_back(in1);
		ret.push_back(in2);
		return ret;
	}

	inline void calculate(){
		this->obj = calcObj(this->cVar, this->iVar);
		this->scale();
		this->calcWeights();
		return;
	}

	inline bool operator==(const Chromosome & second) const{
		if((this->cVar == second.cVar) && (this->iVar == second.iVar)){
			return 1;
		}
		return 0;
	}

/********************************Constructors**********************************/
	Chromosome();										//Random constructor
	Chromosome(std::vector<double>, std::vector<int>);	//Constructor with seed
};

#endif
