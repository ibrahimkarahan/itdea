#ifndef PROBLEM_H
#define PROBLEM_H

#include <vector>
#include <list>
#include <cmath>
#include <iostream>
#include "randomc.h"
#include "globals.h"

void setProblem(int);
void setLimits();
void setCoef();
void setParams(int);

std::vector<double> calcObj(std::vector<double>, std::vector<int>);

#endif
