# An Interactive Territory Defining Evolutionary Algorithm: iTDEA

The full text of the published article is at [IEEE Xplore](http://ieeexplore.ieee.org/xpls/abs_all.jsp?arnumber=5585741&tag=1).

This repository contains the original source code used to run the experiments included in the article. It was originally written in VS2005 and later modified to be built and run on Linux.

What you'll find here is a collection of brittle code that I wrote before I was a better software engineer. It does run but it will also produce lots of junk in the working directory. Proceed with caution.

## Usage
1. Build: ```make```
2. Run: ```./bin/itdea```
3. Clean: ```make clean```

Set of arguments required to run iTDEA can be found in the original article.

## Supported Problems
```src/problem.cpp``` contains the full suite of ZDT (Zitzler et al) and DTLZ (Deb et al) benchmark problems. Edit this file to add new problems.
